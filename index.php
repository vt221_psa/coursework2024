<!DOCTYPE html>
<html lang="uk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ласкаво просимо до нашого магазину одягу</title>
    <link rel="stylesheet" href="/myshop/assets/css/styles.css">
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f5f5f5;
            margin: 0;
            padding: 0;
        }
        .container {
            width: 80%;
            margin: 0 auto;
            padding: 20px;
        }
        .block {
            background-color: #fff;
            border-radius: 8px;
            padding: 20px;
            margin-bottom: 20px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        .block h2 {
            text-align: center;
            color: #2c3e50;
        }
        .block p {
            font-size: 16px;
            line-height: 1.6;
            color: #34495e;
            text-align: justify;
        }
        .btn {
            display: block;
            width: 200px;
            margin: 20px auto;
            padding: 10px 20px;
            text-align: center;
            background-color: #3498db;
            color: #fff;
            text-decoration: none;
            border-radius: 5px;
            transition: background-color 0.3s;
        }
        .btn:hover {
            background-color: #2980b9;
        }
        .slider {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-radius: 10px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2);
        }
        .slides {
            display: flex;
            transition: transform 0.5s ease-in-out;
        }
        .slide {
            min-width: 100%;
            box-sizing: border-box;
        }
        .slide img {
            width: 100%;
            display: block;
        }
        .nav {
            position: absolute;
            top: 50%;
            width: 100%;
            display: flex;
            justify-content: space-between;
            transform: translateY(-50%);
        }
        .nav button {
            background: none;
            border: none;
            font-size: 2em;
            color: white;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <?php
    session_start();
    // Логіка для включення відповідного заголовку залежно від ролі користувача
    if (isset($_SESSION['user_role']) && $_SESSION['user_role'] == 'admin') {
        include __DIR__ . '/views/layout/adminheader.php';
    } else {
        include __DIR__ . '/views/layout/header.php';
    }
    ?>

    <div class="container">
        <div class="slider">
            <div class="slides">
                <div class="slide"><img src="/myshop/assets/images/slider1.webp" alt="Fashion 1"></div>
                <div class="slide"><img src="/myshop/assets/images/slider2.webp" alt="Fashion 2"></div>
                <div class="slide"><img src="/myshop/assets/images/slider3.webp" alt="Fashion 3"></div>
            </div>
            <div class="nav">
                <button id="prev">&#10094;</button>
                <button id="next">&#10095;</button>
            </div>
        </div>

        <div class="block">
            <h2>Наша історія</h2>
            <p>Магазин одягу "Стильний гардероб" був заснований у 2010 році з метою пропонувати модний і якісний одяг для кожного. Ми прагнемо допомогти нашим клієнтам виглядати стильно та відчувати себе впевнено.</p>
            <p>Починаючи з маленького бутика, ми зростали і розширювалися, щоб включити широкий асортимент одягу, взуття та аксесуарів для чоловіків, жінок та дітей. Сьогодні ми пропонуємо модні колекції від провідних дизайнерів.</p>
            <p>У 2015 році ми відкрили наш перший інтернет-магазин, що дозволило нам обслуговувати клієнтів по всій країні. Ми продовжуємо вдосконалювати наш сервіс та розширювати асортимент, щоб задовольнити потреби наших клієнтів.</p>
        </div>

        <div class="block">
            <h2>Наші цінності</h2>
            <p>Ми прагнемо надавати якісні товари, професійні консультації та відмінний сервіс. Наші цінності базуються на моді, якості та турботі про клієнтів.</p>
            <p>Ми завжди ставимо потреби наших клієнтів на перше місце. Наші співробітники постійно проходять навчання, щоб бути в курсі нових модних тенденцій та найкращих практик обслуговування клієнтів.</p>
            <p>Ми підтримуємо місцеві громади та організовуємо благодійні акції, щоб допомогти тим, хто потребує підтримки. Ми віримо, що кожен заслуговує на можливість виглядати та відчувати себе чудово.</p>
        </div>

        <div class="block">
            <h2>Наші товари</h2>
            <p>У нас ви знайдете широкий вибір одягу, взуття та аксесуарів для чоловіків, жінок та дітей. Ми співпрацюємо з провідними брендами, щоб забезпечити нашим клієнтам тільки найкраще.</p>
            <p>Кожен товар проходить ретельну перевірку якості перед тим, як потрапити на наші полиці. Наші консультанти завжди готові допомогти вам з вибором, враховуючи ваші індивідуальні потреби та вподобання.</p>
            <a href="/myshop/views/product/index.php" class="btn">Переглянути каталог</a>
        </div>
    </div>
    
    <?php include __DIR__ . '/views/layout/footer.php'; ?>

    <script>
        let currentIndex = 0;
        const slides = document.querySelector('.slides');
        const totalSlides = slides.children.length;

        document.getElementById('next').addEventListener('click', () => {
            currentIndex = (currentIndex + 1) % totalSlides;
            updateSlidePosition();
        });

        document.getElementById('prev').addEventListener('click', () => {
            currentIndex = (currentIndex - 1 + totalSlides) % totalSlides;
            updateSlidePosition();
        });

        function updateSlidePosition() {
            slides.style.transform = 'translateX(' + (-currentIndex * 100) + '%)';
        }
    </script>
</body>
</html>
